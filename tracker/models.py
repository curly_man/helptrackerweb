from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from .constants import(
    STATUS, IN_PROGRESS,
    PRIORITIES, NORMAL,
)


class Project(models.Model):
    name = models.CharField(max_length=40, default=None)
    users = models.ManyToManyField(User, related_name='shared_users', default=None, null=True, blank=True)
    created_user = models.ForeignKey(User, related_name='created_user', on_delete=models.CASCADE, default=None,
                                     null=True, blank=True)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=10, default=None, null=True)
    user = models.ForeignKey(User, default=None, null=True)
    # task = models.ManyToManyField('Task', null=True)

    def __str__(self):
        return self.name


class Task(models.Model):
    user = models.ForeignKey(User, null=True, default=None)
    tags = models.ManyToManyField(Tag, blank=True, default=None, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, default=None, blank=True, null=True)
    name = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=160, null=True)
    create_at = models.DateTimeField(default=datetime.now())
    deadline = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=1, choices=STATUS, default=IN_PROGRESS)
    priority = models.CharField(max_length=1, choices=PRIORITIES, default=NORMAL)

    def __str__(self):
        return self.name
