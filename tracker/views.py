from django.shortcuts import render, redirect
from .models import Task, Tag, Project
from django.contrib import auth
from .forms import (AddTaskForm, AddTagForm,
                    ChangeTaskForm, AddProjectForm)
from django.core.urlresolvers import reverse
from django.db.models import Q
from .constants import (
    COMPLETED, OVERDUE, IN_PROGRESS, PERIODIC
)


def index(request):
    tasks = Task.objects.filter(Q(status__in=[IN_PROGRESS, PERIODIC]))
    username = auth.get_user(request).username
    context = {
        'tasks': tasks,
        'username': username
    }
    print("hola")
    return render(request, 'index.html', context)


def details(request, id):
    task = Task.objects.get(id=id)
    tags = Tag.objects.filter(task=task)

    context = {
        'task': task,
        'tags': tags
    }

    print("hello")

    return render(request, 'details.html', context)


def add_task(request):
    username = auth.get_user(request).username
    tags = Tag.objects.filter(user=request.user)
    context = {
        'username': username,
    }
    if request.method == 'POST':
        form = AddTaskForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            description = form.cleaned_data['description']
            deadline = form.cleaned_data['deadline']
            user = request.user
            task = Task(user=user, name=name, description=description, deadline=deadline)
            task.save()
            task.tags = form.cleaned_data['tags']
            task.save()
            print("hola")
            return redirect('main.html')
    else:
        form = AddTaskForm()
    context['form'] = form
    return render(request, 'add_task.html', context)


def delete_task(request, id):
    task = Task.objects.get(id=id)
    print('delete')
    if request.method == 'POST':
        task.delete()
    return redirect('main.html')


def complete_task(request, id):
    task = Task.objects.get(id=id)
    if request.method == 'POST':
        task.status = COMPLETED
        task.save()
    return render(request, 'main.html')


def change_task(request, id):
    username = auth.get_user(request).username
    tags = Tag.objects.filter(user=request.user)
    task = Task.objects.get(id=id)
    context = {
        'username': username,
        'tags': tags,
        'task': task
    }
    if request.method == 'POST':
        form = AddTaskForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.name = form.cleaned_data['name']
            task.description = form.cleaned_data['description']
            task.deadline = form.cleaned_data['deadline']
            task.tags = form.cleaned_data['tags']
            task.save()
            return redirect(reverse('main.html'))
    else:
        form = ChangeTaskForm(instance=task, initial={'name': task.name, 'status': task.status})

    context['form'] = form
    return render(request, 'change_task.html', context)


def add_tag(request):
    username = auth.get_user(request).username
    context = {
        'username': username
    }
    print("add")
    if request.method == 'POST':
        form = AddTagForm(request.POST)
        print('qwer')
        if form.is_valid():
            name = form.cleaned_data['name']
            user = request.user
            if not Tag.objects.filter(name=name).exists():
                tag = Tag(name=name, user=user)
                tag.save()
                print('tag')
            return redirect('/tracker/tags')
    else:
        form = AddTagForm()
    context['form'] = form
    return render(request, 'add_tag.html', context)


def show_tags(request):
    tags = Tag.objects.filter(user=request.user)
    context = {
        'tags': tags
    }
    return render(request, 'tags.html', context)


def main(request):
    tasks = Task.objects.filter(Q(user=request.user) & Q(status__in=[IN_PROGRESS, PERIODIC]))
    username = auth.get_user(request).username
    context = {
        'tasks': tasks,
        'username': username
    }
    print("hhh")
    for task in tasks:
        print(task.name)
    return render(request, 'main.html', context)


def main_all_tasks(request):
    tasks = Task.objects.filter(Q(user=request.user))
    username = auth.get_user(request).username
    context = {
        'tasks': tasks,
        'username': username
    }
    print('hola')
    for task in tasks:
        print(task.name)
    return render(request, 'main.html', context)


def main_perform_tasks(request):
    tasks = Task.objects.filter(Q(user=request.user) & Q(status__in=[COMPLETED, PERIODIC]))
    username = auth.get_user(request).username
    context = {
        'tasks': tasks,
        'username': username
    }
    print('hello')
    for task in tasks:
        print(task.name)
    return render(request, 'main.html', context)


def tag_details(request, id):
    username = auth.get_user(request).username
    print('hello')
    print(id)
    tag = Tag.objects.get(id=id)
    tasks = tag.task_set.all()
    tags = Tag.objects.filter(user=request.user)

    context = {
        'tasks': tasks,
        'username': username,
        'tags': tags,
        'tag': tag
    }
    return render(request, 'tag_details.html', context)


def add_project(request):
    username = auth.get_user(request).username
    context = {
        'username': username
    }
    print('project')
    if request.method == 'POST':
        name = request.POST['name']
        user = request.user
        project = Project(name=name, created_user=user)
        project.save()
        project.users.add(user)
        project.save()
        return render(request, 'main.html', context)
    else:
        form = AddTagForm()
    context['form'] = form
    return render(request, 'add_project.html', context)


def show_projects(request):
    projects = Project.objects.filter(Q(created_user=request.user) | Q(users=request.user))
    context = {
        'projects': projects
    }
    print("hopppe")
    return render(request, 'projects.html', context)


def project_details(request, id):
    username = auth.get_user(request).username
    project = Project.objects.get(id=id)
    tasks = project.task_set.all()
    users = project.users.all()
    tags = Tag.objects.filter(user=request.user)
    projects = Project.objects.filter(users=request.user)


    context = {
        'tasks': tasks,
        'users': users,
        'username': username,
        'tags': tags,
        'projects': projects,
        'project': project

    }
    return render(request, 'project_details.html', context)
