from django import forms

from .models import Task, Tag


class AddTaskForm(forms.ModelForm):
    name = forms.CharField()
    deadline = forms.DateField(input_formats=['%d %m %Y'])
    description = forms.CharField(widget=forms.Textarea(attrs={'rows': 4, 'cols': 65}))
    tags = forms.CheckboxSelectMultiple(attrs={'height': 50})

    class Meta:
        model = Task

        fields = ['name', 'description', 'deadline', 'tags', 'priority', 'status']


class ChangeTaskForm(forms.ModelForm):
    name = forms.CharField()
    deadline = forms.DateField(input_formats=['%d %m %Y'])
    description = forms.CharField(widget=forms.Textarea(attrs={'rows': 4, 'cols': 65}))
    tags = forms.CheckboxSelectMultiple(attrs={'height': 50})

    class Meta:
        model = Task

        fields = ['name', 'description', 'deadline', 'tags', 'priority', 'status']


class AddTagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['name']


class AddProjectForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['name']
