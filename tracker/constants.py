LOW = 'L'
NORMAL = 'N'
HIGH = 'H'
PRIORITIES = (
    (LOW, 'Low'),
    (NORMAL, 'Normal'),
    (HIGH, 'High'),
)

IN_PROGRESS = 'I'
COMPLETED = 'C'
OVERDUE = 'O'
PERIODIC = 'P'
STATUS = (
    (IN_PROGRESS, 'In progress'),
    (COMPLETED, 'Completed'),
    (OVERDUE, 'Overdue'),
    (PERIODIC, 'Periodic'),
)